@component('mail::message')
  # Upcoming workshop reminder.

  Hello {{ $attendee->name }}, Thanks a lot for registering for our workshop.


  @component('mail::button', ['url' => 'https://google.com'])
  Workshop Page
  @endcomponent

  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
