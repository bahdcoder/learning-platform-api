@component('mail::message')
  # Course content reviewed.

  Hello {{ $course->user->name }}, Thanks a lot for submitting your course.
  Here's some review for it.

  @component('mail::button', ['url' => route('course.preview', $course->slug)])
  View course
  @endcomponent

  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
