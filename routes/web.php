<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
|
| Here are the routes for authentication.
| login, register, forgot passwords, reset passwords
|
*/
Auth::routes();

/*
|--------------------------------------------------------------------------
| Courses Routes
|--------------------------------------------------------------------------
|
| Api crud routes for /courses
|
*/
Route::apiResource('courses', 'CoursesController');

/*
|--------------------------------------------------------------------------
| Lessons Routes
|--------------------------------------------------------------------------
|
| Api crud routes for /lessons
|
*/
Route::apiResource('/{module}/lessons', 'LessonsController');

/*
|--------------------------------------------------------------------------
| Workshop Routes
|--------------------------------------------------------------------------
|
| Api crud routes for /workshops
|
*/
Route::apiResource('workshops', 'WorkshopsController');

/*
|--------------------------------------------------------------------------
| Attendees Routes
|--------------------------------------------------------------------------
|
| Api crud routes for /attendees
|
*/
Route::apiResource('/{workshop}/attendees', 'AttendeesController');

/*
|--------------------------------------------------------------------------
| Lesson route
|--------------------------------------------------------------------------
|
| api route for getting a single lesson
|
*/
Route::get('lessons/{lesson}', 'LessonsController@show');

/*
|--------------------------------------------------------------------------
| Course route
|--------------------------------------------------------------------------
|
| api route for submitting a course for review
|
*/
Route::put('courses/{course}/submit', 'CourseStatusController@submitForApproval');

/*
|--------------------------------------------------------------------------
| Course route
|--------------------------------------------------------------------------
|
| api route for rejecting a course approval submittion.
|
*/
Route::put('courses/{course}/reject', 'CourseStatusController@rejectApproval');

/*
|--------------------------------------------------------------------------
| Modules route
|--------------------------------------------------------------------------
|
| api route for CRUD /modules.
|
*/
Route::apiResource('/{course}/modules', 'ModulesController');
