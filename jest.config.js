module.exports = {
  testRegex: 'resources/assets/js/test/.*.spec.js$',
  moduleFileExtensions: [
    'js',
    'json',
    'vue'
  ],
  'transform': {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
  },
  collectCoverage: true,
  collectCoverageFrom: [
    'resources/assets/js/**/*.{js,vue}',
    '!**/node_modules/**'
  ]
}
