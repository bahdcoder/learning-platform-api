<?php

namespace App\Jobs;

use Storage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The storage url for the video to be deleted.
     *
     * @var string
     */
    public $video;

    /**
     * Create a new job instance.
     */
    public function __construct(string $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Storage::disk('s3')->delete($this->video);
    }
}
