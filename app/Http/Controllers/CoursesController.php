<?php

namespace App\Http\Controllers;

use Storage;
use App\Models\Course;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    /**
     * Instantiate a new CoursesController instance.
     */
    public function __construct()
    {
        $this->middleware('auth')
             ->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Course::class);

        return auth()->user()->courses()
            ->create($request->only(['title']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return response()->json($course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $this->authorize('update', $course);

        $this->validate($request, [
            'title' => 'sometimes|required|max:255',
            'image' => 'required|image|dimensions:min_width=750,min_height=150',
            'subtitle' => 'required|max:255',
            'description' => 'required',
            'price' => 'sometimes|required|numeric',
            'premium' => 'required|boolean',
        ]);

        $image = $request->image->store('public/courses');

        $course->title = $request->title ? $request->title : $course->title;
        $course->image = Storage::url($image);
        $course->description = $request->description;

        $course->premium = $request->premium;

        $course->price = $request->premium ? $request->price : null;
        $course->subtitle = $request->subtitle;

        $course->save();

        return $course;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
