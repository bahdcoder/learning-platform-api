<?php

namespace App\Http\Controllers;

use Storage;
use App\Models\Module;
use App\Models\Lesson;
use App\Jobs\DeleteVideo;
use Illuminate\Http\Request;

class LessonsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Module              $module
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Module $module, Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:40',
            'description' => 'required|max:255',
            'video' => 'sometimes|required|mimetypes:video/*',
            'video_id' => 'sometimes|required|string',
        ]);

        return $module->lessons()
            ->create([
                'title' => $request->title,
                'description' => $request->description,
                'video' => $request->video_id ?? $request->video->store('lessons', 's3'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Lesson $lesson
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        return response()->json($lesson);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Module       $module
     * @param \App\Models\Less         $lesson
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module, Lesson $lesson)
    {
        $this->authorize('update', $module->course);

        $this->validate($request, [
            'title' => 'sometimes|required|max:40',
            'description' => 'sometimes|required|max:255',
            'video' => 'sometimes|required|mimetypes:video/*',
            'video_id' => 'sometimes|required|string',
        ]);

        if ($request->video || $request->video_id) {
            dispatch(new DeleteVideo($lesson->video));
        }

        $lesson->update([
            'title' => $request->title ?? $lesson->title,
            'description' => $request->description ?? $lesson->description,
            'video' => $request->video_id ?? $request->video->store('lessons', 's3'),
        ]);

        return response()->json($lesson);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Module $module
     * @param \App\Models\Lesson $lesson
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module, Lesson $lesson)
    {
        $this->authorize('delete', $module->course);

        $lesson->delete();

        return response()->json([
            'message' => 'Lesson deleted successfully.',
        ]);
    }
}
