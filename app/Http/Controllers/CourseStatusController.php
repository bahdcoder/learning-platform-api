<?php

namespace App\Http\Controllers;

use Mail;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Mail\CourseContentReviewed;

class CourseStatusController extends Controller
{
    /**
     * Initialize controller.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Submit a course for approval.
     *
     * @param \App\Models\Course $course
     *
     * @return \Illuminate\Http\Response
     */
    public function submitForApproval(Course $course)
    {
        $course->submitForApproval();

        return response()->json($course);
    }

    /**
     * Reject a course approval submittion.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Course       $course
     *
     * @return \Illuminate\Http\Response
     */
    public function rejectApproval(Request $request, Course $course)
    {
        $this->authorize('rejectApproval', $course);

        $review = $course->reviews()->create([
            'content' => $request->review,
            'type' => 'approval',
            'user_id' => auth()->user()->id,
        ]);

        Mail::send(new CourseContentReviewed($review, $course));

        $course->rejectApproval();

        return response()->json($course);
    }
}
