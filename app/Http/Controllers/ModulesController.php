<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Module;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Course       $course
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course)
    {
        $this->validate($request, [
            'title' => 'required|max:40',
        ]);

        return response()->json(
            $course->modules()->create($request->only('title', 'description')),
            201
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Course       $course
     * @param \App\Models\Module       $module
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course, Module $module)
    {
        $this->authorize('update', $course);

        $module->update([
            'title' => $request->title ?? $module->title,
            'description' => $request->description ?? $request->description,
        ]);

        return response()->json($module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Course $course
     * @param \App\Models\Module $module
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course, Module $module)
    {
        $this->authorize('delete', $course);
        $module->delete();

        return response()->json([
            'message' => 'Module deleted successfully.',
        ]);
    }
}
