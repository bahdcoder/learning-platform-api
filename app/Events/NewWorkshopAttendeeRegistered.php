<?php

namespace App\Events;

use App\Models\User;
use App\Models\Workshop;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class NewWorkshopAttendeeRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The attendee.
     *
     * @var \App]Models\User
     */
    public $attendee;

    /**
     * The workshop.
     *
     * @var \App\Models\Workshop
     */
    public $workshop;

    /**
     * Create a new event instance.
     */
    public function __construct(User $attendee, Workshop $workshop)
    {
        $this->attendee = $attendee;
        $this->workshop = $workshop;
    }
}
