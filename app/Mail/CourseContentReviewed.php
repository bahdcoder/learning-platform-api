<?php

namespace App\Mail;

use App\Models\Review;
use App\Models\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CourseContentReviewed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Set the review to be sent for the course.
     *
     * @var \App\Models\Review
     */
    public $review;

    /**
     * Set the course for which the review was created.
     *
     * @var \App\Models\Course
     */
    public $course;

    /**
     * Create a new message instance.
     */
    public function __construct(Review $review, Course $course)
    {
        $this->review = $review;
        $this->course = $course;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.course-content-reviewed');
    }
}
