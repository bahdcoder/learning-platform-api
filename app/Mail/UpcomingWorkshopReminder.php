<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Workshop;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpcomingWorkshopReminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The workshop attendee.
     *
     * @var \App\Models\User
     */
    public $attendee;

    /**
     * The workshop.
     *
     * @var \App\Models\Workshop
     */
    public $workshop;

    /**
     * Create a new message instance.
     */
    public function __construct(User $attendee, Workshop $workshop)
    {
        $this->attendee = $attendee;
        $this->workshop = $workshop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.workshop-reminder');
    }
}
