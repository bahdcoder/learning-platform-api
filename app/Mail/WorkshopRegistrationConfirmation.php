<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Workshop;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WorkshopRegistrationConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The newly registered attendee.
     *
     * @var \App\Models\User
     */
    public $attendee;

    /**
     * The newly registered attendee.
     *
     * @var \App\Models\Workshop
     */
    public $workshop;

    /**
     * Create a new message instance.
     */
    public function __construct(User $attendee, Workshop $workshop)
    {
        $this->attendee = $attendee;
        $this->workshop = $workshop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.workshop-registration-confirmation');
    }
}
