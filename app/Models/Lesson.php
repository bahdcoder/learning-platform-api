<?php

namespace App\Models;

class Lesson extends Model
{
    /**
     * Boot the lesson model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($lesson) {
            $lesson->id = uuid4();
            $lesson->slug = str_slug($lesson->title);
        });
    }

    /**
     * Get the database field to be used for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
