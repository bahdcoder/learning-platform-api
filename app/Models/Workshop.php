<?php

namespace App\Models;

class Workshop extends Model
{
    /**
     * Set fields to be treated as dates.
     *
     * @var string
     */
    protected $dates = [
        'date',
    ];

    /**
     * Boot the course model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($workshop) {
            $workshop->id = uuid4();
            $workshop->slug = str_slug($workshop->title);
        });
    }

    /**
     * Get the database field to be used for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the User Model attendees.
     *
     * @return \Illuminate\Support\Collection $collection of \App\Models\Uer
     */
    public function getAttendees()
    {
        // TODO: Maybe try to find a more laravel-ish way of doing this.
        return User::whereIn(
            'id',
            $this->attendees->pluck('user_id')
        )->get();
    }

    /**
     * Get workshop attendees.
     */
    public function attendees()
    {
        return $this->hasMany(Attendee::class);
    }
}
