<?php

namespace App\Models;

use Carbon\Carbon;

class Course extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * Set fields for mass assignment.
     *
     * @var string
     */
    public $approvalPending = 'approval_pending';

    /**
     * Set fields for mass assignment.
     *
     * @var string
     */
    public $editing = 'editing';

    /**
     * Set fields for mass assignment.
     *
     * @var string
     */
    public $published = 'published';

    /**
     * Define default database attributes.
     *
     * @var array
     */
    protected $attributes = [
        'status' => 'editing',
    ];

    /**
     * Boot the course model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($course) {
            $course->id = uuid4();
            $course->slug = str_slug($course->title);
        });
    }

    /**
     * Set the course status to approval_pending.
     */
    public function submitForApproval()
    {
        $this->status = $this->approvalPending;
        $this->save();
    }

    /**
     * Set the course status to editing.
     */
    public function rejectApproval()
    {
        $this->status = $this->editing;
        $this->save();
    }

    /**
     * Publish a course.
     */
    public function publish()
    {
        $this->status = $this->published;
        $this->published_at = Carbon::now();
        $this->save();
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApprovalPending($query)
    {
        return $query->where('status', $this->approvalPending);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('status', $this->published);
    }

    /**
     * Get the database field to be used for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The course has many modules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    /**
     * The course has many reviews.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
