<?php

namespace App\Models;

class Module extends Model
{
    /**
     * The module has many lessons.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }

    /**
     * The module belongs to a course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
