<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class User extends Authenticatable
{
    use Notifiable, HasRoleAndPermission;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Boot the User model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->id = uuid4();
        });
    }

    /**
     * The user has many courses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    /**
     * The user has many workshops.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workshops()
    {
        return $this->hasMany(Workshop::class);
    }

    /**
     * Add an admin role to a user.
     */
    public function attachAdminRole()
    {
        $adminRole = Role::where('slug', 'admin')->first();
        $this->attachRole($adminRole);
    }

    /**
     * Register a user to attend a workshop.
     */
    public function attendWorkshop(Workshop $workshop)
    {
        $workshop->attendees()->create([
            'user_id' => $this->id,
        ]);
    }
}
