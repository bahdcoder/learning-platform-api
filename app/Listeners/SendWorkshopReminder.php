<?php

namespace App\Listeners;

use Mail;
use App\Mail\UpcomingWorkshopReminder;
use App\Events\NewWorkshopAttendeeRegistered;

class SendWorkshopReminder
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param NewWorkshopAttendeeRegistered $event
     */
    public function handle(NewWorkshopAttendeeRegistered $event)
    {
        Mail::to($event->attendee)
            ->later(
                $event->workshop->date->subDay(),
                new UpcomingWorkshopReminder($event->attendee, $event->workshop)
            );

        Mail::to($event->attendee)
            ->later(
                $event->workshop->date->subHour(),
                new UpcomingWorkshopReminder($event->attendee, $event->workshop)
            );
    }
}
