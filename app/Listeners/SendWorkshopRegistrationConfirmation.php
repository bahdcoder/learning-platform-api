<?php

namespace App\Listeners;

use Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NewWorkshopAttendeeRegistered;
use App\Mail\WorkshopRegistrationConfirmation;

class SendWorkshopRegistrationConfirmation implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param NewWorkshopAttendeeRegistered $event
     */
    public function handle(NewWorkshopAttendeeRegistered $event)
    {
        Mail::to($event->attendee)
            ->send(
                new WorkshopRegistrationConfirmation($event->attendee, $event->workshop)
            );
    }
}
