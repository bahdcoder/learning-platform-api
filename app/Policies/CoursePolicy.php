<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Course;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function view(User $user, Course $course)
    {
    }

    /**
     * Determine whether the user can create courses.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->level() > 1;
    }

    /**
     * Determine whether the user can update the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function update(User $user, Course $course)
    {
        return $user->id === $course->user_id;
    }

    /**
     * Determine whether the user can delete the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function delete(User $user, Course $course)
    {
        return $user->id === $course->user_id;
    }

    /**
     * Determine if a user can review a course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function rejectApproval(User $user, Course $course)
    {
        return $user->hasRole('admin');
    }
}
