<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        /*
         * Add Roles
         *
         */
        if (Role::where('name', '=', 'Admin')->first() === null) {
            Role::create([
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'Admin Role',
                'level' => 5,
            ]);
        }

        if (Role::where('name', '=', 'Instructor')->first() === null) {
            Role::create([
                'name' => 'Instructor',
                'slug' => 'instructor',
                'description' => 'Instructor Role',
                'level' => 3,
            ]);
        }

        if (Role::where('name', '=', 'User')->first() === null) {
            Role::create([
                'name' => 'User',
                'slug' => 'user',
                'description' => 'User Role',
                'level' => 1,
            ]);
        }
    }
}
