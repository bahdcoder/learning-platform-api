<?php

use jeremykenedy\LaravelRoles\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        /*
         * Add Permissions
         *
         */
        if (Permission::where('name', '=', 'Can publish courses')->first() === null) {
            Permission::create([
                'name' => 'Can publish courses',
                'slug' => 'courses.publish',
                'description' => 'Can publish courses',
                'model' => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can review courses')->first() === null) {
            Permission::create([
                'name' => 'Can review courses',
                'slug' => 'courses.review',
                'description' => 'Can review courses',
                'model' => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can review courses')->first() === null) {
            Permission::create([
                'name' => 'Can Edit Users',
                'slug' => 'edit.users',
                'description' => 'Can edit users',
                'model' => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can Delete Users')->first() === null) {
            Permission::create([
                'name' => 'Can Delete Users',
                'slug' => 'delete.users',
                'description' => 'Can delete users',
                'model' => 'Permission',
            ]);
        }
    }
}
