<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('workshops', function (Blueprint $table) {
            $table->string('id');
            $table->string('title');
            $table->string('slug');
            $table->text('embed');
            $table->integer('user_id');
            $table->text('description');
            $table->datetime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('workshops');
    }
}
