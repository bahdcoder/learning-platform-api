<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkShopReminderEmailsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('work_shop_reminder_emails', function (Blueprint $table) {
            $table->string('id');
            $table->string('user_id');
            $table->string('workshop_id');
            $table->datetime('one_hour_to_workshop');
            $table->datetime('twenty_four_hours_to_workshop');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('work_shop_reminder_emails');
    }
}
