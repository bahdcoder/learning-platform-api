<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Course::class, function (Faker $faker) {
    $title = $faker->sentence();

    return [
        'id' => uuid4(),
        'title' => $faker->sentence(),
        'slug' => str_slug($title),
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
    ];
});
