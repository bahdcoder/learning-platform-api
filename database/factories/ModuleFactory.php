<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Module::class, function (Faker $faker) {
    return [
        'id' => uuid4(),
        'title' => $faker->sentence(),
        'description' => $faker->sentence(5),
        'course_id' => function () {
            return factory(\App\Models\Course::class)->create()->id;
        },
    ];
});
