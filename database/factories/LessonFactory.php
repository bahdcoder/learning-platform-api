<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Lesson::class, function (Faker $faker) {
    return [
        'id' => uuid4(),
        'title' => $faker->sentence(),
        'description' => $faker->sentence(),
        'video' => $faker->word(),
        'module_id' => function () {
            return factory(\App\Models\Module::class)->create()->id;
        },
    ];
});
