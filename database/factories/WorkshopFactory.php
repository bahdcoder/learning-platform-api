<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Models\Workshop::class, function (Faker $faker) {
    $title = $faker->sentence();

    return [
        'id' => uuid4(),
        'title' => $faker->sentence(),
        'slug' => str_slug($title),
        'description' => $faker->sentence(),
        'embed' => $faker->sentence(),
        'date' => Carbon::now(),
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
    ];
});
