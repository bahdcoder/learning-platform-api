<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use App\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A course belongs to a user.
     */
    public function testACourseBelongsToAUser()
    {
        $user = factory(User::class)->create();

        $user->courses()->create([
            'title' => 'lorem ipsum',
        ]);

        $this->assertNotNull(Course::where('title', 'lorem ipsum')->first());
    }

    /**
     * A course has many sections.
     */
    public function testACourseHasManyModules()
    {
        $course = factory(Course::class)->create();

        $course->modules()->create([
            'title' => 'lorem ipsum',
            'description' => 'lorem ipsum',
        ]);

        $this->assertNotNull(Module::where('title', 'lorem ipsum')->first());
    }

    /**
     * Update a course status to approval pending.
     */
    public function testSubmitCourseForApproval()
    {
        $course = factory(Course::class)->create();

        $this->assertEquals($course->status, 'editing');

        $course->submitForApproval();

        $this->assertEquals($course->status, 'approval_pending');
    }

    /**
     * Update a course status to editing.
     */
    public function testRejectApproval()
    {
        $course = factory(Course::class)->create();

        $this->assertEquals($course->status, 'editing');

        $course->submitForApproval();

        $this->assertEquals($course->status, 'approval_pending');

        $course->rejectApproval();

        $this->assertEquals($course->status, 'editing');
    }

    /**
     * Test filter to get pending approval courses.
     */
    public function testFilterToGetPendingApprovalCourses()
    {
        $course = factory(Course::class)->create();

        $course2 = factory(Course::class)->create();

        $course->submitForApproval();

        $coursesForApproval = Course::approvalPending()->get();

        $this->assertTrue($coursesForApproval->contains($course));
        $this->assertFalse($coursesForApproval->contains($course2));
    }

    /**
     * Test can publish a course to the public.
     */
    public function testCanPublishACourse()
    {
        $course = factory(Course::class)->create();

        $course2 = factory(Course::class)->create();

        $course->publish();

        $publishedCourses = Course::published()->get();

        $this->assertTrue($publishedCourses->contains($course));
        $this->assertFalse($publishedCourses->contains($course2));
    }
}
