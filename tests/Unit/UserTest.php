<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Workshop;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can attend a workshop.
     */
    public function testAUserCanAttendAWorkshop()
    {
        $user = factory(User::class)->create();

        $workshop = factory(Workshop::class)->create();

        $user->attendWorkshop($workshop);

        $attendees = $workshop->getAttendees();

        $this->assertTrue($attendees->contains($user));
    }
}
