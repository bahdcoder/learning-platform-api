<?php

namespace Tests\Unit;

use Storage;
use Tests\TestCase;
use App\Jobs\DeleteVideo;
use Illuminate\Http\UploadedFile;

class DeleteVideoJobTest extends TestCase
{
    /**
     * The DeleteVideo job deletes a video from storage.
     */
    public function testDeleteVideoJobDeletesVideoFromStorage()
    {
        Storage::fake('s3');

        $video = UploadedFile::fake()->create('lesson-1.avi', 50000);

        Storage::disk('s3')->putFile('lessons', $video);

        Storage::disk('s3')->assertExists("/lessons/{$video->hashName()}");

        (new DeleteVideo("/lessons/{$video->hashName()}"))->handle();

        Storage::disk('s3')->assertMissing("/lessons/{$video->hashName()}");
    }
}
