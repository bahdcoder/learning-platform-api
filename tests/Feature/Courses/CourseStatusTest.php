<?php

namespace Tests\Feature\Courses;

use Mail;
use Artisan;
use Tests\TestCase;
use App\Models\User;
use App\Models\Review;
use App\Models\Course;
use App\Mail\CourseContentReviewed;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseStatusTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A course can be submitted for approval.
     */
    public function testACourseCanBeSubmittedForApproval()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $course = $user->courses()->create([
            'title' => 'lorem ipsum',
        ]);

        $response = $this->json('PUT', "/courses/{$course->slug}/submit");

        $response->assertStatus(200)
            ->assertJSON([
                'title' => 'lorem ipsum',
                'status' => 'approval_pending',
            ]);

        $this->assertEquals(
            Course::find($course->id)->status,
            'approval_pending'
        );
    }

    /**
     * A course approval can be rejected.
     */
    public function testACourseApprovalCanBeRejected()
    {
        Mail::fake();

        $this->withoutExceptionHandling();

        Artisan::call('db:seed');

        $user = factory(User::class)->create();

        $user->attachAdminRole();

        $this->actingAs($user);

        $course = $user->courses()->create([
            'title' => 'lorem ipsum',
            ]);

        $this->json('PUT', "/courses/{$course->slug}/submit")
            ->assertJSON([
                'status' => 'approval_pending',
                ]);

        $response = $this->json('PUT', "/courses/{$course->slug}/reject", [
                        'review' => 'lorem ipsum review',
                    ]);
        $response->assertStatus(200)
            ->assertJSON([
                'title' => 'lorem ipsum',
                'status' => 'editing',
            ]);

        Mail::assertSent(CourseContentReviewed::class, function ($mail) {
            return ($mail->review->content === 'lorem ipsum review')
                && ($mail->review->type === 'approval');
        });

        $this->assertEquals(
            Course::find($course->id)->status,
            'editing'
        );

        $this->assertNotNull(
            Review::first()
        );
    }
}
