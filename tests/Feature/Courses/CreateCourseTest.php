<?php

namespace Tests\Feature\Courses;

use Artisan;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCourseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can create a course.
     */
    public function testUserCanCreateCourse()
    {
        $this->withoutExceptionHandling();

        Artisan::call('db:seed');

        $user = factory(User::class)->create();

        $user->attachAdminRole();

        $this->actingAs($user);

        $response = $this->json('POST', '/courses', [
            'title' => 'Lorem ipsum',
            ]);

        $response->assertStatus(201)
            ->assertJSON([
                'title' => 'Lorem ipsum',
                'slug' => 'lorem-ipsum',
                ]);
    }

    /**
     * A basic test example.
     */
    public function testOnlyAuthenticatedUsersCanCreateCourses()
    {
        $response = $this->json('POST', '/courses', [
            'title' => 'Lorem ipsum',
        ]);

        $response->assertStatus(401)
            ->assertJSON([
                'message' => 'Unauthenticated.',
            ]);
    }
}
