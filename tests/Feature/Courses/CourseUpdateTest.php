<?php

namespace Tests\Feature;

use Storage;
use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseUpdateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can update course details.
     */
    public function testAUserCanUpdateCourseDetails()
    {
        $this->withoutExceptionHandling();
        Storage::fake('public');
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg', 750, 1500);

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'subtitle' => 'subtitle',
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(200)
            ->assertJSON([
                'image' => "/storage/courses/{$image->hashName()}",
                'subtitle' => 'subtitle',
                'description' => 'lorem ipsum',
                'premium' => true,
                'price' => 99,
            ]);

        Storage::disk('local')->assertExists("public/courses/{$image->hashName()}");
    }

    /**
     * Test only course creator can update course.
     */
    public function testOnlyCourseCreatorCanUpdateCourse()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $this->actingAs($user2);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('PUT', "/courses/{$course->slug}", []);

        $response->assertStatus(403);
    }

    /**
     * Image field is required for update.
     */
    public function testTheImageIsRequiredToUpdateCourseDetails()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'subtitle' => 'subtitle',
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJSON([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'image' => ['The image field is required.'],
                ],
            ]);
    }

    /**
     * Image field provided must be an actual image.
     */
    public function testTheImageFieldMustBeAnImageFile()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => 'INVALID_IMAGE_FIELD',
            'subtitle' => 'subtitle',
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJSON([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'image' => ['The image must be an image.'],
                ],
            ]);
    }

    /**
     * Image field is required for update.
     */
    public function testTheImageMustBeAtleast1500X750()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'subtitle' => 'subtitle',
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJSON([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'image' => ['The image has invalid image dimensions.'],
                ],
            ]);
    }

    /**
     * Subtitle field is required for updating course details.
     */
    public function testTheSubtitleFieldIsRequiredForUpdatingCourseDetails()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJSON([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'subtitle' => ['The subtitle field is required.'],
                ],
            ]);
    }

    /**
     * Subtitle field is required for updating course details.
     */
    public function testThePremiumFieldIsRequiredForUpdatingCourseDetails()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'subtitle' => 'subtitle',
            'description' => 'lorem ipsum',
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJSON([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'premium' => ['The premium field is required.'],
                ],
            ]);
    }

    /**
     * Subtitle field is required for updating course details.
     */
    public function testTheSubtitleFieldMustBeLessThan255Characters()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'subtitle' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. 
                It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. 
                Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,
                written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.
                 The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
            'description' => 'lorem ipsum',
            'premium' => true,
            'price' => 99,
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors('subtitle');
    }

    /**
     * The price field must be a number.
     */
    public function testThePriceMustBeANumber()
    {
        //  $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'premium' => true,
            'subtitle' => 'subtitle',
            'price' => 'SOME_STRING',
            'description' => 'lorem ipsum',
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors('price');
    }

    /**
     * The premium field must be a boolean.
     */
    public function testThePremiumFieldMustBeABoolean()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $image = UploadedFile::fake()->image('image.jpg');

        $response = $this->json('PUT', "/courses/{$course->slug}", [
            'image' => $image,
            'premium' => 'SOME_WRONG_TYPE',
            'subtitle' => 'subtitle',
            'price' => 'SOME_STRING',
            'description' => 'lorem ipsum',
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors('premium');
    }
}
