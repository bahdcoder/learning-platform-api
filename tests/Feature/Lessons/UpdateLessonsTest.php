<?php

namespace Tests\Feature\Lessons;

use Queue;
use Storage;
use Tests\TestCase;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\Course;
use App\Jobs\DeleteVideo;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateLessonsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test a user can update lesson details.
     */
    public function testAUserCanUpdateLessonDetails()
    {
        Storage::fake('s3');
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        $response = $this->json('PUT', "/{$module->id}/lessons/{$lesson->slug}", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video_id' => '238948212',
        ]);

        $response
            ->assertStatus(200)
            ->assertJSON([
                'title' => 'lesson title',
                'description' => 'lesson description',
                'video' => '238948212',
            ]);
    }

    /**
     * Test only course creator can update course.
     */
    public function testOnlyCourseCreatorCanUpdateLesson()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $this->actingAs($user2);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        $response = $this->json('PUT', "/{$module->id}/lessons/{$lesson->slug}", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video_id' => '238948212',
        ]);

        $response
            ->assertStatus(403);
    }

    /**
     * Test a user can update lesson details with new video.
     */
    public function testAUserCanUpdateLessonDetailsWithNewVideo()
    {
        Storage::fake('s3');

        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        $video = UploadedFile::fake()->create('lesson-updated.avi', 500000);

        $response = $this->json('PUT', "/{$module->id}/lessons/{$lesson->slug}", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video' => $video,
        ]);

        $response
            ->assertStatus(200)
            ->assertJSON([
                'title' => 'lesson title',
                'description' => 'lesson description',
                'video' => "lessons/{$video->hashName()}",
            ]);

        $this->assertDatabaseHas('lessons', [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video' => "lessons/{$video->hashName()}",
        ]);

        Storage::disk('s3')->assertExists("/lessons/{$video->hashName()}");
    }

    /**
     * Test when a video is uploaded for update a delete job should be queued.
     */
    public function testWhenVideoIsUploadedForUpdatingADeleteJobShouldBeScheduled()
    {
        Storage::fake('s3');
        Queue::fake();

        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        $video = UploadedFile::fake()->create('lesson-updated.avi', 500000);

        $response = $this->json('PUT', "/{$module->id}/lessons/{$lesson->slug}", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video' => $video,
        ]);

        $response->assertStatus(200);

        Storage::disk('s3')->assertExists("/lessons/{$video->hashName()}");
        Queue::assertPushed(DeleteVideo::class, function ($job) use ($lesson) {
            return $job->video === $lesson->video;
        });
    }
}
