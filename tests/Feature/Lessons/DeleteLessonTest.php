<?php

namespace Tests\Feature\Lessons;

use Tests\TestCase;
use App\Models\User;
use App\Models\Module;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteLessonTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can delete a lesson.
     */
    public function testAUserCanDeleteALesson()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $lesson = $this->getLesson($user);

        $response = $this->json('DELETE', "{$lesson[1]->id}/lessons/{$lesson[0]->slug}");

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Lesson deleted successfully.',
            ]);

        $this->assertNull(Lesson::find($lesson[0]->id));
    }

    /**
     * Only creator of a lesson can delete a lesson.
     */
    public function testOnlyCreatorOfALessonCanDeleteALesson()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $lesson = $this->getLesson();

        $response = $this->json('DELETE', "{$lesson[1]->id}/lessons/{$lesson[0]->slug}");

        $response->assertStatus(403);

        $this->assertNotNull(Lesson::find($lesson[0]->id));
    }

    /**
     * Helper function to get a lesson.
     */
    public function getLesson($user = false)
    {
        if (!$user) {
            $user = factory(User::class)->create();
        }

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        return [$lesson, $module];
    }
}
