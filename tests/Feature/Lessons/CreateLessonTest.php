<?php

namespace Tests\Feature\Lessons;

use Storage;
use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use App\Models\Module;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateLessonTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can create a lesson.
     */
    public function testAUserCanCreateALesson()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video_id' => '238948212',
        ]);

        $response
            ->assertStatus(201)
            ->assertJSON([
                'title' => 'lesson title',
                'description' => 'lesson description',
                'video' => '238948212',
            ]);
    }

    /**
     * A user can upload a video for a lesson.
     */
    public function testAUserCanUploadAVideoForALesson()
    {
        Storage::fake('s3');

        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create();

        $video = UploadedFile::fake()->create('lesson-1.mp4', 5000);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video' => $video,
        ]);

        $response->assertStatus(201)
            ->assertJSON([
                'title' => 'lesson title',
                'description' => 'lesson description',
                'video' => "lessons/{$video->hashName()}",
            ]);

        Storage::disk('s3')->assertExists("/lessons/{$video->hashName()}");
    }

    /**
     * If video is uploaded.
     */
    public function testAVideoIfUploadedShouldBeAValidVideoFormat()
    {
        Storage::fake('s3');

        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $video = UploadedFile::fake()->create('lesson-1.mp4', 50000);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'title' => 'lesson title',
            'description' => 'lesson description',
            'video' => $video,
        ]);

        Storage::disk('s3')->assertExists("/lessons/{$video->hashName()}");
    }

    /**
     * A title is required to create a lesson.
     */
    public function testATitleIsRequiredToCreateALesson()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'description' => 'lesson description',
            'video' => '238948212',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['title']);
    }

    /**
     * A description is required to create a lesson.
     */
    public function testADescriptionIsRequiredToCreateALesson()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'title' => 'lesson title',
            'video' => 238948212,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('description');
    }

    /**
     * A description must be less than 255 characters.
     */
    public function testDescriptionMustBeLessThan255Characters()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'title' => 'lesson title',
            'video' => 238948212,
            'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. 
            It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. 
            Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia,
            written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.
             The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('description');
    }

    /**
     * A title must be less than 40 characters.
     */
    public function testTitleMustBeLessThan40Characters()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $response = $this->json('POST', "/{$module->id}/lessons", [
            'description' => 'lesson title',
            'video' => 238948212,
            'title' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. 
            It has roots in a piece of classical Latin literature from 45 BC',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('title');
    }
}
