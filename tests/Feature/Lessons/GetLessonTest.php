<?php

namespace Tests\Feature\Lessons;

use Tests\TestCase;
use App\Models\User;
use App\Models\Lesson;
use App\Models\Module;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetLessonTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Get a single lesson.
     */
    public function testCanGetSingleLessonData()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
        ]);

        $module = factory(Module::class)->create([
            'course_id' => $course->id,
        ]);

        $lesson = factory(Lesson::class)->create([
            'module_id' => $module->id,
        ]);

        $response = $this->json('GET', "/lessons/{$lesson->slug}");

        $response
            ->assertStatus(200)
            ->assertJSON([
                'id' => $lesson->id,
                'title' => $lesson->title,
            ]);
    }
}
