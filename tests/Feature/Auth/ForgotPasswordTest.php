<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * User forgot password test.
     */
    public function testForgotPasswordEndpointShouldReturnConfirmationMessageOfSentEmail()
    {
        $this->withoutExceptionHandling();
        $fakeUser = factory(User::class)->create();

        $response = $this->json('POST', '/password/email', [
            'email' => $fakeUser->email,
        ]);
        $response->assertStatus(200)
            ->assertJSON([
                'message' => 'Reset password email sent successfully.',
            ]);
    }
}
