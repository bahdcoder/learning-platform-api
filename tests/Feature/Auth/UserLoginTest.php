<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserLoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     */
    public function testAUserCanLogiNAndReceiveUserDetails()
    {
        $fakeUser = factory(User::class)->create();

        $response = $this->json('POST', '/login', [
            'email' => $fakeUser->email,
            'password' => 'secret',
        ]);

        $response->assertStatus(200)
            ->assertJSON([
                'name' => $fakeUser->name,
                'email' => $fakeUser->email,
            ]);
    }
}
