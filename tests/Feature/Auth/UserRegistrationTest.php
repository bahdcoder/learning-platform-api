<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRegistrationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the user can register a new account.
     */
    public function testAUserCanRegisterANewAccount()
    {
        $fakeUser = [
            'name' => 'bahdcoder',
            'email' => 'bahdcoder@gmail.com',
            'password' => 'password',
        ];

        $response = $this->json('POST', '/register', [
            'name' => 'bahdcoder',
            'email' => 'bahdcoder@gmail.com',
            'password' => 'password',
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'name' => $fakeUser['name'],
                'email' => $fakeUser['email'],
            ]);
    }
}
