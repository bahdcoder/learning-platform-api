<?php

namespace Tests\Feature;

use DB;
use Mail;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     */
    public function testUserReceivesConfirmationMessageAfterSuccessResettingPassword()
    {
        $this->markTestSkipped(
            'Revisit this test to make sure user receives confirmation message after resetting password.'
        );

        Mail::fake();

        $user = factory(User::class)->create();

        $this->json('POST', '/password/email', [
            'email' => $user->email,
        ]);

        dd(DB::select('select * from password_resets where email = :email', [
            'email' => $user->email,
        ]));
    }

    /**
     * A basic test example.
     */
    public function testUserReceivesConfirmationMessageAfterFailureResettingPassword()
    {
        $this->markTestSkipped(
            'Revisit this test to make sure user receives error message after failed resetting password.'
        );
    }
}
