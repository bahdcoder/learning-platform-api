<?php

namespace Tests\Feature\Workshop;

use Artisan;
use Tests\TestCase;
use App\Models\User;
use App\Models\WorkShop;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateWorkshopTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Administrator can create a workshop.
     */
    public function testAdminCanCreateAWorkshop()
    {
        $this->withoutExceptionHandling();

        $user = $this->loginAdmin();

        $response = $this->json('POST', '/workshops', [
            'title' => 'lorem ipsum',
            'embed' => 'embed-code-url',
            'date' => '2019-02-11 00:00:00',
            'description' => 'lorem ipsum description',
        ]);

        $response
            ->assertStatus(201)
            ->assertJSON([
                'title' => 'lorem ipsum',
                'embed' => 'embed-code-url',
                'description' => 'lorem ipsum description',
                'date' => '2019-02-11 00:00:00',
            ]);

        $this->assertNotNull(WorkShop::first());
    }

    /**
     * Test only admin can create a workshop.
     */
    public function testOnlyAdminCanCreateAWorkshop()
    {
        $user = $this->actingAs(factory(User::class)->create());

        $response = $this->json('POST', '/workshops', [
            'title' => 'lorem ipsum',
            'embed' => 'embed-code-url',
            'date' => '2019-19-11 00:00:00',
            'description' => 'lorem ipsum description',
        ])->assertStatus(403);

        $this->assertNull(WorkShop::first());
    }

    /**
     * Create and login an admin user.
     */
    public function loginAdmin()
    {
        Artisan::call('db:seed');

        $user = factory(User::class)->create();

        $user->attachAdminRole();

        $this->actingAs($user);

        return $user;
    }
}
