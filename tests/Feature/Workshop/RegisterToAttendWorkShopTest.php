<?php

namespace Tests\Feature\Workshop;

use Mail;
use Event;
use Tests\TestCase;
use App\Models\User;
use App\Models\Workshop;
use App\Models\Attendee;
use App\Mail\UpcomingWorkshopReminder;
use App\Mail\WorkshopRegistrationConfirmation;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterToAttendWorkShopTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can register for workshop.
     */
    public function testAUserCanRegisterForAWorkshop()
    {
        $this->markTestSkipped(
            'The fact that we can\' test the Mail::later() function sucks. Till I find a work around, we\'ll skip this.'
        );
        // TODO: find a way to assert that event was dispatched without
        // preventing the Model events to be triggered.
        //  Event::fake();
        Mail::fake();

        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $workshop = factory(Workshop::class)->create();

        $this->actingAs($user);

        $response = $this->json('POST', "{$workshop->slug}/attendees", []);

        $response->assertStatus(200)
            ->assertJSON([
                'message' => 'Registration to workshop successful.',
            ]);

        Mail::assertSent(WorkshopRegistrationConfirmation::class, function ($mail) use ($user) {
            return $mail->attendee->id === $user->id;
        });

        Mail::assertQueued(UpcomingWorkshopReminder::class, function ($mail) use ($user) {
            return $mail->attendee->id === $user->id;
        });

        Mail::assertQueued(UpcomingWorkshopReminder::class, function ($mail) use ($user) {
            return $mail->attendee->id === $user->id;
        });

        $this->assertNotNull(Attendee::first());
    }
}
