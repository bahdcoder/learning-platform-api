<?php

namespace Tests\Feature\Modules;

use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateModulesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can create a module.
     */
    public function testCanCreateAModuleForACourse()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create(['user_id' => $user->id]);

        $response = $this->json('POST', "/{$course->slug}/modules", [
            'title' => 'lorem ipsum',
            'description' => 'lorem ipsum desc',
        ]);

        $response->assertStatus(201)->assertJSON([
            'title' => 'lorem ipsum',
            'description' => 'lorem ipsum desc',
        ]);

        $this->assertEquals($course->modules->count(), 1);
    }

    /**
     * A user must provide a module title.
     */
    public function testTheTitleIsRequiredToCreateAModule()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create(['user_id' => $user->id]);

        $response = $this->json('POST', "/{$course->slug}/modules", [
            'description' => 'lorem ipsum desc',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['title']);

        $this->assertEquals($course->modules->count(), 0);
    }

    /**
     * A user must provide a module title with less than 40 characters.
     */
    public function testTheTitleOfAModuleMustBeLessThan40Characters()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create(['user_id' => $user->id]);

        $response = $this->json('POST', "/{$course->slug}/modules", [
            'title' => 'lorem ipsum some title lorem ipsum some title
                lorem ipsum some title lorem ipsum some title lorem ipsum some title',
            'description' => 'lorem ipsum desc',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['title']);

        $this->assertEquals($course->modules->count(), 0);
    }
}
