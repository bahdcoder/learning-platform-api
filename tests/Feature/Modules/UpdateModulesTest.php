<?php

namespace Tests\Feature\Modules;

use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use App\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateModulesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can update a module.
     */
    public function testCanUpdateAModule()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create(['user_id' => $user->id]);

        $module = factory(Module::class)->create(['course_id' => $course->id]);

        $response = $this->json('PUT', "/{$course->slug}/modules/{$module->id}", [
            'title' => 'lorem ipsum title updated',
        ]);

        $response
            ->assertStatus(200)
            ->assertJSON(['title' => 'lorem ipsum title updated']);

        $this->assertEquals($course->modules->first()->title, 'lorem ipsum title updated');
    }
}
