<?php

namespace Tests\Feature\Modules;

use Tests\TestCase;
use App\Models\User;
use App\Models\Course;
use App\Models\Module;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteModulesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can delete a module.
     */
    public function testCanDeleteAModule()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $course = factory(Course::class)->create(['user_id' => $user->id]);

        $module = factory(Module::class)->create(['course_id' => $course->id]);

        $response = $this->json('DELETE', "/{$course->slug}/modules/{$module->id}");

        $response
            ->assertStatus(200)
            ->assertJSON(['message' => 'Module deleted successfully.']);

        $this->assertNull($course->modules->first());
    }
}
