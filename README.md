# BAHDCASTS V2
[![Build Status](https://travis-ci.com/bahdcoder/bahdcasts-v2.svg?token=2MrUvo9xLHfRFFNkqyjG&branch=master)](https://travis-ci.com/bahdcoder/bahdcasts-v2)
[![codecov](https://codecov.io/gh/bahdcoder/bahdcasts-v2/branch/master/graph/badge.svg?token=D8vT4suYN1)](https://codecov.io/gh/bahdcoder/bahdcasts-v2)

### Staging server

- [Bahdcasts Staging Server link](https://staging.bahdcasts.com)

## Technology stack

- Laravel
- Vuejs

## How to setup locally
- ```git clone https://github.com/bahdcoder/bahdcasts-v2.git```
- ``` composer install ```
- ``` npm install ```
- ```cp .env.example .env```
- ``` php artisan key:generate ```
<br>
<br>
After setting up database,

 - ```php artisan migrate```
<br>
<br>
To start a dev server for the application,
 - ``` php artisan serve```

## Code styling

- eslint-standard for javascript linting
- psr-2 for php linting

## How to run tests
 - Server tests 
    - ```composer test```
 - Client tests
    - ```npm test```
## Contributing

- When writing commit messages, `composer run-checks` command will be called, and this will run all tests for the application, making sure they all pass. This command will also run eslint, and run php-code-sniffer, to make sure that both the client and server coding style standards were respected. If any of these checks fail, the commit message will aborted. 
- When running `npm test`, eslint will be run on all files, making sure the style guide was respected completely.